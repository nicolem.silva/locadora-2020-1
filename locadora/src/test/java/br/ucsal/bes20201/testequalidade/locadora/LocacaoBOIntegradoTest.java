package br.ucsal.bes20201.testequalidade.locadora;

import br.ucsal.bes20201.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Locacao;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;
import builder.LocacaoBuilder;
import builder.VeiculoBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LocacaoBOIntegradoTest {

	/**
	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano de
	 * fabricaçao e 3 veículos com 8 anos de fabricação.
	 */
	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {
		LocacaoBuilder locacaoBuilder = LocacaoBuilder.umaLocacao();
		VeiculoBuilder veiculoBuilder = VeiculoBuilder.umVeiculo().ano(2012).situacao(SituacaoVeiculoEnum.DISPONIVEL);;

		Veiculo veiculo1 = veiculoBuilder.placa("NIC1111").ano(2019).diaria(150.00).build();
		Veiculo veiculo2 = veiculoBuilder.placa("MAI0505").ano(2019).diaria(150.00).build();
		Veiculo veiculo3 = veiculoBuilder.placa("PAS9595").ano(2012).diaria(50.00).build();
		Veiculo veiculo4 = veiculoBuilder.placa("LOP2626").ano(2012).diaria(50.00).build();
		Veiculo veiculo5 = veiculoBuilder.placa("SIL0405").ano(2012).diaria(50.00).build();

		Locacao locacao1 = locacaoBuilder.umaLocacao().comVeiculos(veiculo1, veiculo2, veiculo3,veiculo4, veiculo5).quantidadeDiasLocacao(3).build();

		Double saidaEsperada = 1305.00;
		Double saidaAtual = LocacaoBO.calcularValorTotalLocacao(locacao1.getVeiculos(), locacao1.getQuantidadeDiasLocacao());

		Assertions.assertEquals(saidaEsperada, saidaAtual);
	}


}
